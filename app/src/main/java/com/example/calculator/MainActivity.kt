package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.calculator.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private var input1 = 0
    private var input2 = 0
    private var inputResult = 0
    private var operatorType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnTambah.setOnClickListener {
            changeOperator("+")
            operatorType = OperatorType.TAMBAH
        }


        binding.btnKurang.setOnClickListener {
            changeOperator("-")
            operatorType = OperatorType.KURANG
        }

        binding.btnKali.setOnClickListener {
            changeOperator("*")
            operatorType = OperatorType.KALI
        }

        binding.btnBagi.setOnClickListener {
            changeOperator("/")
            operatorType = OperatorType.BAGI
        }

        binding.btnResult.setOnClickListener {

            calculate()

        }
    }

    private fun calculate() {
        input1 = binding.etInput1
            .text// mengambil text dari edit text
            .toString()// merubah text ke string
            .toInt()// merubah string ke integer
        input2 = binding.etInput2
            .text
            .toString()
            .toInt()

        when (operatorType) {
            OperatorType.TAMBAH -> {
                tambah()
                // penambahan
            }
            OperatorType.KURANG -> {
                kurang()
                // pengurangan
            }
            OperatorType.KALI -> {
                kali()
                // perkalian
            }
            OperatorType.BAGI -> {
                bagi()
                // pembagian
            }

        }

        binding.tvResult.text = inputResult.toString()
    }

    private fun changeOperator(operator: String) {
        binding.tvOperator.text = operator
    }

    private fun tambah() {
        inputResult = input1 + input2
    }

    private fun kurang() {
        inputResult = input1 - input2
    }

    private fun kali() {
        inputResult = input1 * input2
    }

    private fun bagi() {
        inputResult = input1 / input2
    }

    companion object
}